#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2017 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate code for the single-zone project.
#//!
#///////////////////////////////////////////////////////////////////////////////

ifndef NUCNET_TARGET
NUCNET_TARGET = ../nucnet-tools-code
endif

ifndef WN_USER_TARGET
WN_USER_TARGET = ../wn_user2
endif

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = http://svn.code.sf.net/p/nucnet-tools/code/trunk

BUILD_DIR = $(WN_USER_TARGET)/build

VENDORDIR = $(BUILD_DIR)/vendor
OBJDIR = $(BUILD_DIR)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user

ifndef H5
H5 = h5c++
endif

ifndef FC
FC = gfortran
endif

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# Includes.
#===============================================================================

include $(BUILD_DIR)/Makefile
include $(BUILD_DIR)/Makefile.sparse
include $(USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR)

#===============================================================================
# Add local directory to includes.
#===============================================================================

CFLAGS += -I ./ -I $(WN_USER_TARGET)

#===============================================================================
# Debugging, if desired.
#===============================================================================

ifdef DEBUG
  CFLAGS += -DDEBUG
  FFLAGS += -DDEBUG
endif

#===============================================================================
# wn_user.
#===============================================================================

ifdef WN_USER
  CFLAGS += -DWN_USER
endif

ifdef HYDRO_EVOLVE
  CFLAGS += -DHYDRO_EVOLVE
endif

#===============================================================================
# Objects.
#===============================================================================

SINGLE_ZONE_OBJ = $(WN_OBJ)          \
                  $(SP_OBJ)          \
                  $(WN_USER_OBJ)     \
                  $(NNT_OBJ)         \
                  $(SOLVE_OBJ)       \
                  $(HD5_OBJ)         \
                  $(USER_OBJ)        \
                  $(OPTIONS_OBJ)     \
                  $(ILU_OBJ)         \
                  $(SINGLE_OBJ)      \

SINGLE_ZONE_DEP = $(SINGLE_ZONE_OBJ)

CFLAGS += -DSPARSKIT2
SINGLE_ZONE_DEP += sparse
FLIBS= -L$(SPARSKITDIR) -lskit
CLIBS += -lgfortran

#===============================================================================
# Executables.
#===============================================================================

SINGLE_ZONE_EXEC = single_zone_network

$(SINGLE_ZONE_EXEC): $(SINGLE_ZONE_DEP)
	$(HC) -c -o $(OBJDIR)/$@.o $@.cpp
	$(HC) $(SINGLE_ZONE_OBJ) -o $(BINDIR)/$@ $(OBJDIR)/$@.o $(FLIBS) $(CLIBS)

.PHONY all_single_zone: $(SINGLE_ZONE_EXEC)

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_single_zone cleanall_single_zone

clean_single_zone:
	rm -f $(SINGLE_ZONE_OBJ)

cleanall_single_zone: clean_single_zone
	rm -f $(BINDIR)/$(SINGLE_ZONE_EXEC) $(BINDIR)/$(SINGLE_ZONE_EXEC).exe

clean_nucnet:
	rm -fr $(NUCNET_TARGET)
