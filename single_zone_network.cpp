////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for running a single zone network calculation.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the network and zone data.
  //============================================================================

  wn_user::network_data my_network_data( v_map );

  wn_user::zone_data my_zone_data( v_map, my_network_data.getNucnet() );

  //============================================================================
  // Create evolver, hydro, time_adjuster, limiter, outputter, modifier,
  // properties_updater, nse_corrector, screener, step_printer, and registerer.
  //============================================================================

  wn_user::network_time my_network_time( v_map );

  wn_user::network_evolver my_evolver( v_map );

  wn_user::time_adjuster my_time_adjuster( v_map );

  wn_user::network_limiter my_limiter( v_map );

  wn_user::outputter my_output( v_map );

  wn_user::matrix_modifier my_matrix_modifier( v_map );

  wn_user::properties_updater my_properties_updater( v_map );

  wn_user::rate_modifier my_rate_modifier( v_map );

  wn_user::rate_registerer my_rate_registerer( v_map );

  wn_user::nse_corrector my_nse_corrector( v_map );

  wn_user::screener my_screener( v_map );

  wn_user::step_printer my_step_printer( v_map );

  wn_user::hydro my_hydro( v_map );

  //============================================================================
  // Create zone to evolve.
  //============================================================================

  nnt::Zone zone = my_zone_data.createZone();

  //============================================================================
  // Initialize times.
  //============================================================================

  my_network_time.initializeTimes( zone );

  //============================================================================
  // Rate date updater.
  //============================================================================

  my_rate_registerer( my_network_data.getNetwork() );
  my_rate_registerer.setZoneDataUpdater( zone );

  //============================================================================
  // Screener.
  //============================================================================

  my_screener( zone );

  //============================================================================
  // NSE correction.
  //============================================================================

  my_nse_corrector( zone );

  //============================================================================
  // Initial rate modifier.
  //============================================================================

  my_rate_modifier( zone );

  //============================================================================
  // Initialize hydro.
  //============================================================================

  my_hydro.initialize( zone );

  //============================================================================
  // Initial limiter.
  //============================================================================

  my_limiter( zone );

  //============================================================================
  // Initialize properties.
  //============================================================================

  my_properties_updater.initialize( zone );

  //============================================================================
  // Set the output.
  //============================================================================

  my_output.set( my_network_data.getNucnet() );

  //============================================================================
  // Evolve network while t < final t.
  //============================================================================

  while( my_network_time.isLessThanEndTime( zone ) )
  {

  //============================================================================
  // Update time.
  //============================================================================

    zone.updateProperty(
      nnt::s_TIME,
      zone.getProperty<double>( nnt::s_TIME ) +
        zone.getProperty<double>( nnt::s_DTIME )
    ); 

  //============================================================================
  // Update temperature and density.
  //============================================================================

    my_hydro( zone );

  //============================================================================
  // Modify matrix.
  //============================================================================

    my_matrix_modifier( zone );

  //============================================================================
  // Modify rates (and print those modified).
  //============================================================================

    my_rate_modifier( zone );

  //============================================================================
  // Evolve abundances.
  //============================================================================

    my_evolver( zone );

  //============================================================================
  // Update properties.
  //============================================================================

    my_properties_updater( zone );

  //============================================================================
  // Output.
  //============================================================================

    my_output( zone );

  //============================================================================
  // Print to screen.
  //============================================================================

    my_step_printer( zone );

  //============================================================================
  // Update timestep.
  //============================================================================

    my_time_adjuster( zone );

    my_hydro.adjustTimeStep( zone );

  //============================================================================
  // Limit network.
  //============================================================================

    my_limiter( zone );

  }

  //============================================================================
  // Output.
  //============================================================================

  my_output.write();

  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
